//
//  ViewController.m
//  ScoreCounter2
//
//  Created by jpeger on 2/28/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)player1Changed:(id)sender {
    UIStepper* temp = (UIStepper*)sender;
    self.player1Lbl.text = [NSString stringWithFormat:@"%.f",temp.value];
}

- (IBAction)player2Changed:(id)sender {
    UIStepper* temp = (UIStepper*)sender;
    self.player2Lbl.text = [NSString stringWithFormat:@"%.f",temp.value];
}
@end
