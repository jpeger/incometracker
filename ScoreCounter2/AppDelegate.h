//
//  AppDelegate.h
//  ScoreCounter2
//
//  Created by jpeger on 2/28/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

