//
//  ViewController.h
//  ScoreCounter2
//
//  Created by jpeger on 2/28/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *player1Lbl;
@property (strong, nonatomic) IBOutlet UILabel *player2Lbl;

@property (strong, nonatomic) IBOutlet UIStepper *player1Stepper;
@property (strong, nonatomic) IBOutlet UIStepper *player2Stepper;

- (IBAction)player1Changed:(id)sender;
- (IBAction)player2Changed:(id)sender;

@end

